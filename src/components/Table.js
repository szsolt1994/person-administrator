import React from "react";

class Table extends React.Component {


    constructor(props) {
        super(props);
        this.props = props;
        this.state = {
            data: props.data
        };
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {

    }

    render() {
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">
                            <span className="personName">Name</span>
                            <span className="personJob">Job Title</span>
                        </th>
                        <th scope="col">Age</th>
                        <th scope="col">Nickname</th>
                        <th scope="col">Employee</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        this.props.data.map((row, index) =>
                            <tr key={index} className="align-middle">
                                <th scope="row">
                                    <span className="personName">{row.name}</span>
                                    <span className="personJob">{row.job}</span>
                                </th>
                                <td>{row.age}</td>
                                <td>{row.nick}</td>
                                <td>{row.employee}</td>
                                <td>
                                    <a href="" onClick={this.handleClick}>Delete</a>
                                </td>
                            </tr>
                        )
                    }
                </tbody>
            </table>
        );
    }
}

export default Table;
