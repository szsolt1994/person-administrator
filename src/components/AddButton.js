import React from "react";

export const Button = (props) => {
    return (
        <button className="btn btn-success pull-left" data-toggle=" modal" data-target="#exampleModalLong">
            {props.text}
        </button>
    );
}