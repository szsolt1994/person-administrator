import React, { Component } from 'react';

import { Button } from './components/AddButton.js';
import { DataDump } from './components/DataDump.js';
import Table from './components/Table.js';

import './App.css';
import Data from './data/persons.json';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col-1 mb-5 mt-5 d-block">
              <Button text="Add" />
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <Table data={Data} />
            </div>
          </div>
          <div className="row">
            <div className="col-2 d-block">
              <p>Data dump</p>
            </div>
            <div className="col-12 d-block mb-5">
              <DataDump />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
